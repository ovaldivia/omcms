<?php

?>

<?include_once "views/helpers/header.html.php"?>





    <div class="container contact" style="height: 600px;
    width: 100%;
    margin-top: 62px;
    background-color: white;">

        <h1>Contact Us</h1>

        <div class="row">

            <div class="col-md-12">
                <div class="contact-form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
                        </div>
                    </div>
                    <br><br><br>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                        </div>
                    </div>
                    <br><br><br>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="comment">Comment:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" style="margin-top: 10px;float: right;">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>




    </div>

<?include_once "views/helpers/footer.html.php"?>
